const fs = require('fs');

const stringify = require('./stringify');
const parse = require('./parse');

const runStringify = async (obj, sparePath, mainPath) => {
  mainPath
    ? (await stringify(obj, mainPath)) || (await stringify(obj, sparePath))
    : await stringify(obj, sparePath);
};

const runParse = async (sparePath, mainPath) => {
  const resultObj = mainPath
    ? (await parse(mainPath)) || (await parse(sparePath))
    : await parse(sparePath);

  await removeFile(mainPath);
  await removeFile(sparePath);
  return resultObj;
};

const removeFile = async path => {
  if (fs.stat(path, err => err)) {
    fs.unlink(path, err => {
      if (err) throw err;
      console.log(`File ${path} has been deleted`);
    });
  }
};

const persistentAdapter = async (sparePath, mainPath, dataObj) =>
  dataObj
    ? await runStringify(dataObj, sparePath, mainPath)
    : await runParse(sparePath, mainPath);

module.exports = persistentAdapter;
