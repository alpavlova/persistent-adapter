const fs = require('fs');
const chai = require('chai');

const expect = chai.expect;
chai.should();

const chaiAsPromised = require('chai-as-promised');
const chaiStream = require('chai-stream');

chai.use(chaiAsPromised);
chai.use(chaiStream);

const persistentAdapter = require('../main');

describe('Write stream', () => {
  it('should return a writable stream', () => {
    const writeStream = fs.createWriteStream('test.json');

    expect(writeStream).to.be.a.WritableStream;
  });

  it('should return a stream that will end', () => {
    const writeStream = fs.createWriteStream('test.json');

    writeStream.write('test');
    writeStream.end();

    expect(writeStream)
      .to.end.then(() => true)
      .catch(err => err);
  });

  it('should return promise resolve', () => {
    const result = persistentAdapter('test2.json', 'test1.json', { a: 1 });

    expect(result).to.be.a('promise');
    result.should.be.fulfilled;
  });

  it('should return object { a: 1} from test1.json', async () => {
    await persistentAdapter('test2.json', 'test1.json', { a: 1 });
    const stream = await persistentAdapter('test2.json', 'test1.json');

    expect(stream).to.be.an('object');
    expect(stream).to.deep.equal({ a: 1 });
  });

  it('should return object { a: 1} from test2.json', async () => {
    await persistentAdapter('test2.json', null, { a: 1 });
    const stream = await persistentAdapter('test2.json', 'test1.json');

    expect(stream).to.be.an('object');
    expect(stream).to.deep.equal({ a: 1 });
  });
});

describe('Read stream', () => {
  it('should return a readable stream', () => {
    const readStream = fs.createReadStream('test.json');

    expect(readStream).to.be.a.ReadableStream;
  });

  it('should return a stream that will end', () => {
    const readStream = fs.createReadStream('test.json');

    expect(readStream)
      .to.end.then(() => true)
      .catch(err => err);
  });

  it('should return promise resolve', () => {
    const result = persistentAdapter('test2.json', 'test1.json');

    expect(result).to.be.a('promise');
    result.should.eventually.deep
      .equal({ a: 1 })
      .then(() => true)
      .catch(err => err);
  });
});
