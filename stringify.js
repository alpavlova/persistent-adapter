const fs = require('fs');

const stringify = async (dataObj, path) => {
  const string = JSON.stringify(dataObj);

  return new Promise((resolve, reject) => {
    const writeStream = fs.createWriteStream(path);

    writeStream.write(string);
    writeStream.end();
    writeStream.on('finish', resolve);
    writeStream.on('error', reject);
  })
    .then(() => true)
    .catch(err => {
      console.log('This stream failed:', err.source);
      console.log('Original error was:', err.originalError);
    });
};

module.exports = stringify;
