module.exports = {
    "parser": "babel-eslint",
    "extends": [
        "airbnb-base",
        "plugin:prettier/recommended",
    ],
    "env": {
        "es6": true,
        "node": true,
        "jest": true,
        "jasmine": true
    },
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module",
        "ecmaFeatures": {
            "generators": false,
            "objectLiteralDuplicateProperties": false,
            "experimentalObjectRestSpread": true
        }
    },
    "rules": {
        "no-use-before-define": "off",
        "no-restricted-syntax": "off",
        "no-empty": [2, { "allowEmptyCatch": true }],
        "import/prefer-default-export": "off",
        "no-underscore-dangle": "off",
        "prettier/prettier": "error",
        "object-shorthand": ["error", "always", {
            "ignoreConstructors": false,
            "avoidQuotes": false,
        }],
    },
    "settings": {
        "import/resolver": {
            "node": {
                "moduleDirectory": [
                    "node_modules",
                ]
            }
        }
    }
};
