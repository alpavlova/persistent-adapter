const fs = require('fs');

const parse = async path => {
  const chunksArr = [];

  return new Promise((resolve, reject) => {
    fs.createReadStream(path)
      .on('data', chunk => chunksArr.push(chunk))
      .on('end', () => resolve(JSON.parse(chunksArr)))
      .on('error', reject);
  })
    .then(result => result)
    .catch(err => {
      console.log('This stream failed:', err.source);
      console.log('Original error was:', err.originalError);
    });
};

module.exports = parse;
